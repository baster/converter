ARG NODE_VER=14

FROM node:${NODE_VER}-alpine AS node-builder

WORKDIR /var/www/frontend
COPY ./frontend/package.json ./frontend/package-lock.json /var/www/frontend/

# Install packages
RUN npm ci --force

COPY frontend /var/www/frontend/

RUN npm run build

FROM php:8.0.3-fpm as php

ENV PHPREDIS_VERSION="5.3.2" \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS="0" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="10000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="192" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10"

RUN apt-get update && apt-get install -y git make libzip-dev zip curl libpq5 libpq-dev; \
    docker-php-ext-install zip pdo pdo_pgsql; \
    apt-get autoremove --purge -y libpq-dev; \
    apt-get clean; \
    rm -rf /var/lib/apt

RUN pecl install redis-$PHPREDIS_VERSION;\
    pecl install; \
    docker-php-ext-enable redis

RUN curl -sS https://getcomposer.org/installer | php -- \
    --filename=composer --install-dir=/usr/local/bin; \
    echo "alias composer='composer'" >> /root/.bashrc

COPY docker/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

WORKDIR /app

COPY ./composer.json ./composer.lock ./symfony.lock /app/

RUN composer install --no-ansi --no-scripts --no-interaction --no-progress

COPY bin /app/bin
COPY config /app/config
COPY src /app/src
COPY public /app/public
COPY migrations /app/migrations

RUN composer run-script post-install-cmd
RUN composer run-script post-update-cmd
RUN composer dump-autoload --optimize

COPY --from=node-builder /var/www/frontend/ /app/frontend/