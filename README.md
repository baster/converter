[![pipeline status](https://gitlab.com/baster/converter/badges/main/pipeline.svg)](https://gitlab.com/baster/converter/-/commits/main)

[![coverage report](https://gitlab.com/baster/converter/badges/main/coverage.svg)](https://gitlab.com/baster/converter/-/commits/main)


# Symfony 5 Currency converter

## CI/CD description
https://docs.google.com/document/d/1dOsXhQMBYoiPFn0jPbodMziWFpCWRuV9HGze8phnYLg/edit?usp=sharing

## Implementations

- [x] Environment in Docker
- [x] Rest API
- [x] Angular
- [x] Swagger API Doc

## Use Cases

- [x] Import quotes from ECB and Coinbase (
  https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
  https://api.coindesk.com/v1/bpi/historical/close.json
  )
- [x] CRUD quotes
- [x] Currency converter

## Stack

- PHP 8
- Postgres 11
- Angular 11

## Commands

|    Action        	|     Command    |
|------------------	|---------------	|
|  Setup 	          | `make start`   |
|  Run Tests       	| `make phpunit` |
|  Run Import       	| `make import` |

