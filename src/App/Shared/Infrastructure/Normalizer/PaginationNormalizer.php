<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Normalizer;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PaginationNormalizer implements ContextAwareNormalizerInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param $object
     * @param string|null $format
     * @param array $context
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $items = [];

        foreach ((array)$object->getItems() as $item) {
            $items[] = $this->normalizer->normalize($item, $format, $context);
        }

        return [
            'items' => $items,
            'pagination' => [
                'count' => $object->count(),
                'total' => $object->getTotalItemCount(),
                'perPage' => $object->getItemNumberPerPage(),
                'page' => $object->getCurrentPageNumber(),
                'pages' => ceil($object->getTotalItemCount() / $object->getItemNumberPerPage()),
            ],
        ];
    }

    /**
     * @param mixed $data
     * @param array<mixed> $context
     */
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof PaginationInterface;
    }
}
